/**
 * Created by zhangxinyang on 8/14/17.
 */


function parseJSON(data) {
    var status = data.status;
    tokens = [];
    beforeSpaces = [];
    var beginIdx = -1;
    if (status === "failure") return;
    $.each(data.articles[0].sentences, function(i, sentence) {
        $.each(sentence.tokens, function(i, token) {
            var begIdx = token.beginIndex;
            if (beginIdx === -1) {
                beginIdx = begIdx;
            }
            beforeSpaces.push(begIdx - beginIdx);
            tokens.push(token.word);
            beginIdx = token.endIndex;
        });
    });
}

function readFromJSON(path) {
     return $.getJSON(path, parseJSON);
}

function renderText(selector, tokens, beforeSpaces, span) {
    $(selector).empty();
    var n = tokens.length;
    var start = -1, end = -1, span_node = null;
    if (span !== null) {
        start = span[0];
        end = span[1];
        span_node = $(document.createElement("span"));
        $(selector).append(span_node);
        span_node.addClass("answer");
    }
    for (var i = 0; i < n; i++) {
        var obj = $(selector);
        if (i >= start && i < end)
            obj = span_node;
        obj.append(document.createTextNode(" ".repeat(beforeSpaces[i])));
        obj.append(document.createTextNode(tokens[i]));
    }
}

function renderQuestion(path) {
    return $.getJSON(path, function (data) {
        $.each(data, function(i, question) {
            var li_node = $(document.createElement("li"));
            li_node.addClass("question");
            li_node.text(question.question);
            li_node.data("start", question.start);
            li_node.data("end", question.end);
            $("#list-question").append(li_node);
        });
    });
}
